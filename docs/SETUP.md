## Run Local

* Ruby version: **2.7.1**

* System dependencies

    
* Configuration
     - Postgres: config/database.yml

### Setup Requirements
- **Ruby** greater than version **(2.6.5)**.
- **bunder** installed.
- PostgreSQL


### Setup
* Database creation

```
rake db:create db:migrate
```

* Libraries install
```
bundle install
```

### Running Tests
```
bundle exec rake spec
```

### Running the application
```
bundle exec rails s
