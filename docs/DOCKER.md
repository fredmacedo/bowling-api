## Docker Setup

* Build images
```
docker-compose build
```
* Load the database
```
docker-compose run bowling-api rake db:create db:migrate
```

* Run the test suite
```
docker-compose run bowling-api rake spec
```
* Run services
```
docker-compose up
```
