# API Docs

## POST Game /api/games

Method responsible to create a new game.

#### Request Example
```
curl --location --request POST 'http://localhost:3000/api/games'
```
##### Response (201)
```
{
  "data": {
    "id": "19",
    "type": "game",
    "attributes": {
      "id": 19
    }
  }
}
```

## GET Game /api/games/:id

Method responsible to get Game scores

#### Request Example
```
curl --location --request GET 'http://localhost:3000/api/games/:id
```

##### Response (200)
```
{
    "data": {
        "id": "19",
        "type": "game_score",
        "attributes": {
            "id": 19,
            "current_frame": 10,
            "frames": [
                {
                    "frame": 1,
                    "balls": [
                        5,
                        5
                    ],
                    "frame_score": 14,
                    "accumulated_score": 14
                },
                {
                    "frame": 2,
                    "balls": [
                        4,
                        4
                    ],
                    "frame_score": 8,
                    "accumulated_score": 22
                },
                {
                    "frame": 3,
                    "balls": [
                        10
                    ],
                    "frame_score": 20,
                    "accumulated_score": 42
                },
                {
                    "frame": 4,
                    "balls": [
                        9,
                        1
                    ],
                    "frame_score": 15,
                    "accumulated_score": 57
                },
                {
                    "frame": 5,
                    "balls": [
                        5,
                        5
                    ],
                    "frame_score": 20,
                    "accumulated_score": 77
                },
                {
                    "frame": 6,
                    "balls": [
                        10
                    ],
                    "frame_score": 30,
                    "accumulated_score": 107
                },
                {
                    "frame": 7,
                    "balls": [
                        10
                    ],
                    "frame_score": 26,
                    "accumulated_score": 133
                },
                {
                    "frame": 8,
                    "balls": [
                        10
                    ],
                    "frame_score": 10,
                    "accumulated_score": 143
                },
                {
                    "frame": 9,
                    "balls": [
                        6,
                        4
                    ],
                    "frame_score": 10,
                    "accumulated_score": 153
                },
                {
                    "frame": 10,
                    "balls": [],
                    "frame_score": 0,
                    "accumulated_score": 0
                }
            ],
            "total": 153
        }
    }
}
```

Giving an Inexisting ID you should get an error
```
{
    "errors": [
        "Game not found"
    ]
}
```

## POST Play Ball /api/games/:id/play

Method responsible to play a new ball at the game

##### Request Example
```
url --location --request POST '{{host}}/api/games/:id/play' \
--data-raw '{ "pins_dropped": 4 }'
```

##### Response (200)
```
{
    "data": {
        "id": "19",
        "type": "game_score",
        "attributes": {
            "id": 19,
            "current_frame": 10,
            "frames": [
                {
                    "frame": 1,
                    "balls": [
                        5,
                        5
                    ],
                    "frame_score": 14,
                    "accumulated_score": 14
                },
                {
                    "frame": 2,
                    "balls": [
                        4,
                        4
                    ],
                    "frame_score": 8,
                    "accumulated_score": 22
                },
                {
                    "frame": 3,
                    "balls": [
                        10
                    ],
                    "frame_score": 20,
                    "accumulated_score": 42
                },
                {
                    "frame": 4,
                    "balls": [
                        9,
                        1
                    ],
                    "frame_score": 15,
                    "accumulated_score": 57
                },
                {
                    "frame": 5,
                    "balls": [
                        5,
                        5
                    ],
                    "frame_score": 20,
                    "accumulated_score": 77
                },
                {
                    "frame": 6,
                    "balls": [
                        10
                    ],
                    "frame_score": 30,
                    "accumulated_score": 107
                },
                {
                    "frame": 7,
                    "balls": [
                        10
                    ],
                    "frame_score": 26,
                    "accumulated_score": 133
                },
                {
                    "frame": 8,
                    "balls": [
                        10
                    ],
                    "frame_score": 10,
                    "accumulated_score": 143
                },
                {
                    "frame": 9,
                    "balls": [
                        6,
                        4
                    ],
                    "frame_score": 10,
                    "accumulated_score": 153
                },
                {
                    "frame": 10,
                    "balls": [],
                    "frame_score": 0,
                    "accumulated_score": 0
                }
            ],
            "total": 153
        }
    }
}
```

Example Play Ball with error:

If you try to drop more than acceptable pins you should receive an error message.

#### Request Example
```
curl --location --request POST '{{host}}/api/games/:id/play' \
--data-raw '{ "pins_dropped": 11 }'
```
##### Response (422)
```
{
  "errors": [
    "Play can't drop more than 10 pins"
  ]
}
```
If you try to play a new ball at the end of the game you should get an error response.
##### Response (422)
```
{
    "errors": [
        "Game has ended"
    ]
}
```
