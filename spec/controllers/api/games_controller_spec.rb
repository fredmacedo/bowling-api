require 'rails_helper'
RSpec.describe Api::GamesController, type: :controller do
  describe "POST /api/games" do
    it 'should return 201 response' do
      post :create
      expect(response.status).to eq(201)
    end
    it 'should create a new game' do
      expect {
        post :create
      }.to change(Game, :count).by(1)
    end
    it 'should create 10 frames' do
      expect {
        post :create
      }.to change(Frame, :count).by(10)
    end
    it 'should return a Game JSON' do
      post :create
      game = Game.last
      json_response = GameSerializer.new(game).serialized_json
      expect(response.body).to eq(json_response)
    end
  end
  describe "GET /api/games/:id" do
    context "giving an invalid ID" do
      it 'should return 404 response' do
        post :show, params: { id: :invalid }
        expect(response.status).to eq(404)
      end
    end
    context "giving a valid ID" do
      let(:game) { create(:game_with_frames)}
      let(:game_score) { GameScore.new(game: game) }
      let(:json_response) { GameScoreSerializer.new(game_score).serialized_json }
      it 'should return 200 response' do
        post :show, params: { id: game.id }
        expect(response.status).to eq(200)
      end
      it 'should return a GameScore JSON' do
        post :show, params: { id: game.id }
        expect(response.body).to eq(json_response)
      end
    end
  end
  describe "POST /api/games:id/play" do
    context "giving an invalid ID" do
      it 'should return 404 response' do
        post :play, params: { id: :invalid }
        expect(response.status).to eq(404)
      end
    end
    context "giving a valid ID" do
      let(:game) { create(:game_with_frames)}
      let(:game_score) { GameScore.new(game: game) }
      context "with valid arguments" do
        let(:pins_dropped) { 5 }
        let(:json_response) { GameScoreSerializer.new(game_score).serialized_json }
        it 'should return 200 response' do
          post :play, params: { id: game.id, pins_dropped: pins_dropped }
          expect(response.status).to eq(200)
        end
        it 'should return a GameScore JSON' do
          post :play, params: { id: game.id, pins_dropped: pins_dropped }
          expect(response.body).to eq(json_response)
        end
      end
      context "with invalid pins dropped" do
        let(:pins_dropped) { 11 }
        let(:error_message) { {
          errors: ["Play can't drop more than 10 pins"]
        }}
        it 'should return 422 response' do
          post :play, params: { id: game.id, pins_dropped: pins_dropped }
          expect(response.status).to eq(422)
        end
        it 'should return the error message' do
          post :play, params: { id: game.id, pins_dropped: pins_dropped }
          expect(response.body).to eq(error_message.to_json)
        end
      end
      context "with invalid throw at the lastest frame" do
        let(:last_frame) { game.frames.last }
        let(:error_message) { {
          errors: ["Play can't drop more than 10 pins"]
        }}
        before :each do
          last_frame.update(score: [5, 4])
          game.update(frame: last_frame)
        end
        let(:pins_dropped) { 4 }
        it 'should return 422 response' do
          post :play, params: { id: game.id, pins_dropped: pins_dropped }
          expect(response.status).to eq(422)
        end
      end
    end
  end

  describe "END TO END Tests" do
    END_TO_END = {
      plays: [
        [8, 2, 5, 4, 4, 4, 3, 3, 2, 2, 3, 7, 5, 4, 5, 4, 10, 4, 3],
        [3, 6, 6, 4, 5, 5, 6, 2, 10, 10, 7, 3, 5, 1, 8, 1, 10, 9, 1],
        [5, 3, 4, 6, 10, 10, 10, 5, 4, 9, 1, 3, 0, 0, 1, 10, 4, 6],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 5, 3, 4, 6, 7, 2, 1, 1],
        [0, 4, 6, 4, 5, 3, 10, 9, 0, 10, 2, 7, 10, 10, 5, 4],
        [10, 0, 0, 3, 7, 0, 10, 4, 4, 6, 0, 5, 3, 9, 1, 3, 0, 7, 3, 4]
      ],
      total_expected: [99, 145, 148, 54, 136, 86]
    }
    END_TO_END[:plays].each_with_index do |play, idx|
      it "should run end to end(#{idx}) and check total" do
        post :create
        game = Game.last
        play.each do |pin|
          post :play, params: { id: game.id, pins_dropped: pin }
        end
        game = Game.last
        game_score = GameScore.new(game: game)
        expect(game_score.total).to eq(END_TO_END[:total_expected][idx])
      end
    end
  end
end
