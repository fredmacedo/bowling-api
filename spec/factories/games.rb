FactoryBot.define do
  factory :game do
    after(:build) do |game|
      class << game
        def start; true; end
      end
    end
  end
  factory :game_with_frames, class: Game do
  end
end
