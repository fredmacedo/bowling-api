FactoryBot.define do
  factory :frame do
    game { create(:game) }
    index { 0 }
    score { [] }
  end
end
