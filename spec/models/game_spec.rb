require 'rails_helper'

RSpec.describe Game, type: :model do
  describe "#after_create" do
    describe "#start" do
      it 'should call start' do
      game = Game.new
      expect(game).to receive(:start)
      game.save
    end
    end
    describe "#crete_frames" do
      it 'should create 10 frames' do
      g = Game.new
      g.save
      expect(g.frames.size).to eq(10)
    end
    end
    it 'should assign current frame as the first frame' do
      g = Game.new
      g.save
      expect(g.frames.first).to eq(g.frame)
    end
  end
  describe "#play" do
    let(:game) { create(:game_with_frames)}
    let(:pins_dropped) { 5 }
    context "with frame play not been successful" do
      let(:last_frame) { game.frames.last }
      context "and you try to throw an invalid turn" do
        before :each do
          game.frame = last_frame
          game.frame.score = [10, 5, 4, 2]
        end
        it 'should have error' do
          game.play(pins_dropped)
          expect(game.errors.messages[:base]).to eq(["Game has ended"])
        end
        it 'should return false' do
          expect(game.play(pins_dropped)).to be_falsey
        end
      end
    end
    context "with frame play successful" do
      context "and it's the first ball at frame different than last" do
        it 'should receive play_on_next_frame? call' do
          expect(game).to receive(:play_on_next_frame?)
          game.play(pins_dropped)
        end
        it 'should not receive a save call' do
          expect(game).to_not receive(:save)
          game.play(pins_dropped)
        end
        it 'should receive calculate_pending call' do
          expect(game).to receive(:calculate_pending).with(0, pins_dropped)
          game.play(pins_dropped)
        end
        it 'should return true' do
          expect(game.play(pins_dropped)).to be_truthy
        end
      end
      context "and it's the second ball at frame different than last" do
        before :each do
          game.frame.score = [4]
        end
        it 'should receive play_on_next_frame? call' do
          expect(game).to receive(:play_on_next_frame?)
          game.play(pins_dropped)
        end
        it 'should receive a save call' do
          expect(game).to receive(:save)
          game.play(pins_dropped)
        end
        it 'should receive calculate_pending call' do
          expect(game).to receive(:calculate_pending).with(0, pins_dropped)
          game.play(pins_dropped)
        end
        it 'should return true' do
          expect(game.play(pins_dropped)).to be_truthy
        end
      end
    end
  end
  describe "#play_on_next_frame?" do
    let(:game) { create(:game_with_frames)}
    let(:last_frame) { game.frames.last }
    context "and you are at the last frame" do
      before :each do
        game.frame = last_frame
      end
      it 'should return false' do
        expect(game.play_on_next_frame?).to be_falsey
      end
    end
    context "and you are not at the last frame" do
      context "and you haven't played" do
        it 'should return false' do
          expect(game.play_on_next_frame?).to be_falsey
        end
      end
      context "and you have played your second ball" do
        before :each do
          game.frame.score = [4,6]
        end
        it 'should return true' do
          expect(game.play_on_next_frame?).to be_truthy
        end
      end
    end
  end
  describe "#add_extra_score" do
    let(:game) { create(:game_with_frames)}
    let(:pins_dropped) { 5 }
    it "should delegate extra score to frame by index" do
      expect(game.frames[0]).to receive(:add_extra_score).with(pins_dropped)
      game.add_extra_score(0, pins_dropped)
    end
  end
  describe "#calculate_pending" do
    let(:game) { create(:game_with_frames)}
    let(:pins_dropped) { 5 }
    context "giving index 0" do
      it 'should not call add_extra_score' do
        expect(game).to_not receive(:add_extra_score).with(0, pins_dropped)
        game.calculate_pending(0, pins_dropped)
      end
    end
    context "giving index 1" do
      it 'should call add_extra_score once' do
        expect(game).to receive(:add_extra_score).with(0, pins_dropped)
        game.calculate_pending(1, pins_dropped)
      end
    end
    context "giving index 2" do
      it 'should call add_extra_score twice' do
        expect(game).to receive(:add_extra_score).with(0, pins_dropped)
        expect(game).to receive(:add_extra_score).with(1, pins_dropped)
        game.calculate_pending(2, pins_dropped)
      end
    end
  end
  describe "#throw_after_end_game?" do
    let(:game) { create(:game_with_frames)}
    context "giving current frame as last frame" do
      let(:last_frame) { game.frames.last }
      before :each do
        game.frame = last_frame
      end
      context "and you tried to throw an invalid turn" do
        before :each do
          game.frame.score = [10, 10, 10, 2]
        end
        it 'should return true' do
          expect(game.throw_after_end_game?).to be_truthy
        end
      end
      context "and you tried to throw at valid turn" do
        before :each do
          game.frame.score = [10, 10, 10]
        end
        it 'should return true' do
          expect(game.throw_after_end_game?).to be_falsey
        end
      end
    end
    context "giving current frame is not last frame" do
      it 'should return false' do
        expect(game.throw_after_end_game?).to be_falsey
      end
    end
  end
end
