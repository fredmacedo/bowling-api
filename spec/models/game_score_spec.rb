require 'rails_helper'
RSpec.describe GameScore, type: :model do
  describe "#initialize" do
    let(:game) { create(:game_with_frames) }
    context "without game" do
      let(:game_score) { GameScore.new }
      it 'should have total equal 0' do
        expect(game_score.total).to eq(0)
      end
      it 'should have empty frames' do
        expect(game_score.frames).to eq([])
      end
    end
    context "with giving game" do
      let(:game_score) { GameScore.new(game: game)}
      it 'should have total equal 0' do
        expect(game_score.total).to eq(0)
      end
      it 'should include frame 1 with empty score' do
        expect(game_score.frames).to include(frame: 1, balls: [], frame_score: 0, accumulated_score: 0)
      end
      it 'should have 10 frames' do
        expect(game_score.frames.size).to eq(10)
      end
    end
  end
end
