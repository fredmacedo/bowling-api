require 'rails_helper'

RSpec.describe Frame, type: :model do
  let(:frame) { create(:frame) }

  describe "#validation" do
    context "giving you tried to throw more than max allowed throws" do
      before :each do
        allow(frame).to receive(:max_throws).and_return(2)
        allow(frame).to receive(:current_throw).and_return(3)
      end
      it 'should add frame error' do
        frame.save
        expect(frame.errors.messages[:base]).to eq(["Frame can't throw more than 2 balls"])
      end
    end
    context "giving max throws equal 2 (Frame different 10)" do
      before :each do
        allow(frame).to receive(:max_throws).and_return(2)
      end
      context "and you have tried to add more than 10 at the score" do
        before :each do
          frame.score = [5,7]
        end
        it 'should add frame error' do
          frame.save
          expect(frame.errors.messages[:base]).to eq(["Play can't drop more than 10 pins"])
        end
      end
      context "and you have add score with number less than 10" do
        before :each do
          frame.score = [5,4]
        end
        it 'should not have frame errors' do
          frame.save
          expect(frame.errors.any?).to be_falsey
        end
      end
    end
    context "giving Max throws equal 3 (Frame 10)" do
      before :each do
        allow(frame).to receive(:max_throws).and_return(3)
      end
      context "and you have tried to have more than 10 at current throw" do
        before :each do
          frame.score = [4,5,11]
        end
        it 'should add frame error' do
          frame.save
          expect(frame.errors.messages[:base]).to eq(["Play can't drop more than 10 pins"])
        end
      end
      context "and you tried to not have more than 10 at current throw" do
        before :each do
          frame.score = [4,5,10]
        end
        it 'should add frame error' do
          frame.save
          expect(frame.errors.any?).to be_falsey
        end
      end
    end
  end
  describe "#score" do
    it "should have default score as an Array" do
      expect(frame.score).to be_a(Array)
    end
  end
  describe "#extra_score" do
    it "should have default extra_score as an Array" do
      expect(frame.score).to be_a(Array)
    end
  end
  context "#current_throw" do
    it 'should be the size of the score' do
      frame.score = [1,2]
      expect(frame.current_throw).to eq(2)
    end
  end
  describe "#play" do
    context "without errors" do
      it 'should change score' do
        expect {
        frame.play(5)
        }.to change(frame, :score).from([]).to([5])
      end
      it 'should call save' do
        expect(frame).to receive(:save)
        frame.play(5)
      end
    end
    context "with errors" do
      before :each do
        frame.play(11)
      end
      it 'should call clear errors' do
        expect(frame.errors).to receive(:present?)
        expect(frame.errors).to receive(:clear)
        frame.play(5)
      end
      it 'should change the score with error' do
        frame.play(5)
        expect(frame.score).to eq([5])
      end
      it 'should call save' do
        expect(frame).to receive(:save)
        frame.play(5)
      end
      it 'should save successfully' do
        expect(frame.play(5)).to be_truthy
      end
    end
  end
  describe "#total_score" do
    it 'should be the sum of score and total score Arrays' do
      frame.score = [10,0]
      frame.extra_score = [6,2]
      expect(frame.total_score).to eq(18)
    end
  end
  describe '#has_pending_calc?' do
    context "giving you have a strike at the frame" do
      before :each do
        frame.score = [10]
      end
      context "and you have not thrown the second ball after the Strike" do
        before :each do
          frame.extra_score = [5]
        end
        it 'should return true' do
          expect(frame.has_pending_calc?).to be_truthy
        end
      end
      context "and you have thrown the second ball after the Strike" do
        before :each do
          frame.extra_score = [5, 5]
        end
        it 'should return false' do
          expect(frame.has_pending_calc?).to be_falsey
        end
      end
    end
    context "giving you have a spare at the frame" do
      before :each do
        frame.score = [2,8]
      end
      context "and you have not thrown the first ball after the Spare" do
        before :each do
          frame.extra_score = []
        end
        it 'should return true' do
          expect(frame.has_pending_calc?).to be_truthy
        end
      end
      context "and you have thrown the first ball after the Spare" do
        before :each do
          frame.extra_score = [5]
        end
        it 'should return false' do
          expect(frame.has_pending_calc?).to be_falsey
        end
      end
    end
  end
  describe "#add_extra_score" do
    context "giving you have a pending calculation" do
      before :each do
        allow(frame).to receive(:has_pending_calc?).and_return(true)
      end
      it 'should change extra_score' do
        expect {
          frame.add_extra_score(5)
        }.to change(frame, :extra_score).from([]).to([5])
      end
    end
    context "giving you have not pending calculation" do
      before :each do
        allow(frame).to receive(:has_pending_calc?).and_return(false)
      end
      it 'should not change extra_score' do
        frame.add_extra_score(5)
        expect(frame.extra_score).to eq([])
      end
    end
  end
  describe "#is_spare?" do
    context "giving it's the first ball" do
      before :each do
        frame.score = [6]
      end
      it 'should be false' do
        expect(frame.is_spare?).to be_falsey
      end
    end
    context "giving it's the second ball" do
      context "and sum is not 10" do
        before :each do
          frame.score = [6, 3]
        end
        it 'should be false' do
          expect(frame.is_spare?).to be_falsey
        end
      end
      context "and sum is 10" do
        before :each do
          frame.score = [6, 4]
        end
        it 'should be true' do
          expect(frame.is_spare?).to be_truthy
        end
      end
    end
  end
  describe "#is_strike?" do
    context "giving the first ball different 10" do
      before :each do
        frame.score = [5]
      end
      it 'should be false' do
        expect(frame.is_strike?).to be_falsey
      end
    end
    context "giving the first ball equal 10" do
      before :each do
        frame.score = [10]
      end
      it 'should be true' do
        expect(frame.is_strike?).to be_truthy
      end
    end
  end
  describe "#is_last_frame?" do
    context "index is not equal to 10" do
      before :each do
        frame.index = 3
      end
      it 'should be false' do
        expect(frame.is_last_frame?).to be_falsey
      end
    end
    context "index equal 9 (Frame 10)" do
      before :each do
        frame.index = 9
      end
      it 'should be 3' do
        expect(frame.is_last_frame?).to be_truthy
      end
    end
  end
  describe "#has_next_ball?" do
    context "giving you have done a Strike" do
      before :each do
        frame.score = [10]
      end
      it 'should return false' do
        expect(frame.has_next_ball?).to be_falsey
      end
    end
    context "giving you not done a Strike" do
      context "and you have played once" do
        before :each do
          frame.score = [5]
        end
        it 'should return true' do
          expect(frame.has_next_ball?).to be_truthy
        end
      end
      context "and you have played twice" do
        before :each do
          frame.score = [5,4]
        end
        it 'should return false' do
          expect(frame.has_next_ball?).to be_falsey
        end
      end
    end
  end
  describe "#max_throws" do
    context "index is not at the latest frame" do
      before :each do
        frame.index = 3
      end
      it 'should be 2' do
        expect(frame.max_throws).to be(2)
      end
    end
    context "index at the latest frame" do
      before :each do
        frame.index = 9
      end
      context "and is not a spare or strike" do
        before :each do
          frame.score =[ 3, 6 ]
        end
        it 'should be 3' do
          expect(frame.max_throws).to be(2)
        end
      end
      context "and is a spare" do
        before :each do
          frame.score =[ 3, 7 ]
        end
        it 'should be 3' do
          expect(frame.max_throws).to be(3)
        end
      end
      context "and is a strike" do
        before :each do
          frame.score =[ 10 ]
        end
        it 'should be 3' do
          expect(frame.max_throws).to be(3)
        end
      end
    end
  end
end
