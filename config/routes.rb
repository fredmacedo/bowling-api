Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    resources :games, only: [:create, :show] do
      member do
        post "play"
      end
    end

  end
end
