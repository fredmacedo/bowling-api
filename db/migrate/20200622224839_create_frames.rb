class CreateFrames < ActiveRecord::Migration[6.0]
  def change
    create_table :frames do |t|
      t.references :game, null: false, foreign_key: true
      t.integer :index
      t.jsonb :score
      t.jsonb :extra_score
      t.timestamps
    end
  end
end
