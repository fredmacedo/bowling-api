class AddCurrentFrameToGame < ActiveRecord::Migration[6.0]
  def change
    add_reference :games, :frame, index: true
  end
end
