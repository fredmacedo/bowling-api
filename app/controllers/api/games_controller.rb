class Api::GamesController < ApplicationController
  def create
    game = Game.create
    render json: GameSerializer.new(game).serialized_json, status: 201
  end

  def show
    begin
      game = Game.find(params[:id])
      game_score = GameScore.new(game: game)
      render json: GameScoreSerializer.new(game_score).serialized_json
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: ["Game not found"] }, status: 404
    end
  end

  def play
    begin
      game = Game.find(params[:id])
      if game.play(game_params[:pins_dropped])
        game_score = GameScore.new(game: game)
        render json: GameScoreSerializer.new(game_score).serialized_json
      else
        errors = game.errors.any? ? {
          errors: game.errors[:base].as_json
        } : {
          errors: game.frame.errors[:base].as_json
        }
        render json: errors, status: 422
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: ["Game not found"] }, status: 404
    end
  end

  def game_params
    params.permit(:pins_dropped)
  end
end
