class GameScoreSerializer
  include FastJsonapi::ObjectSerializer
  attributes :id, :current_frame, :frames, :total
end
