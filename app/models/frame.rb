class Frame < ApplicationRecord
  belongs_to :game

  validate :validation

  def validation
    errors[:base] << "Frame can't throw more than #{max_throws} balls" if self.current_throw > max_throws
    if max_throws > 2
      errors[:base] << "Play can't drop more than 10 pins" if self.score[current_throw - 1] > 10
    else
      errors[:base] << "Play can't drop more than 10 pins" if self.score[0..1].sum > 10
    end
  end

  # total pins dropped: Array of max size 2
  def score
    self[:score] ||=[]
  end

  # Additional score when it's Strike or Spare
  # when it's Strike: Must receive 2 items at the Array
  # when it's Spare: Must receive 1 item at the Array
  def extra_score
    self[:extra_score] ||=[]
  end

  def current_throw
    score.size
  end

  def play(pins_dropped)
    if self.errors.present?
      self.errors.clear
      self.score[self.score.size - 1] = pins_dropped.to_i
    else
      self.score << pins_dropped.to_i
    end
    save
  end

  # Sum of additional score and throw ball score
  def total_score
    score.sum + extra_score.sum
  end

  # Check if has pending additional scores when
  # is a strike or spare
  def has_pending_calc?
    if is_strike?
      extra_score.size < 2
    else
      is_spare? && extra_score.size == 0
    end
  end

  def add_extra_score(value)
    if has_pending_calc?
      self.extra_score << value
      self.save
    end
  end

  def is_spare?
    return false if current_throw === 1
    score[0..1].sum === 10
  end

  def is_strike?
    score.first === 10
  end

  def is_last_frame?
    index === 9
  end

  def has_next_ball?
    is_strike? ? false : current_throw < 2
  end

  # Frames can support 3 or 2 throws so this method is responsible to calculate
  # max throws
  def max_throws
    if is_last_frame?
      return is_strike? || is_spare? ? 3 : 2
    end
    return 2
  end
end
