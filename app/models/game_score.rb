class GameScore
  include ActiveModel::Model
  attr_accessor :id, :game, :current_frame, :frames, :total

  def initialize(attributes={})
    super
    self.frames = []
    self.total = 0
    load_frames_score if game
  end

  def load_frames_score
    self.id = game.id
    self.current_frame = game.frame.index + 1
    game.frames.order('index asc').each_with_index {|e|
      acc_score = 0
      if (e.index - 1) >= 0 && e.score.size > 0
        acc_score = self.frames[e.index - 1][:accumulated_score]
      end
      self.frames << { frame: e.index + 1, balls: e.score, frame_score: e.total_score, accumulated_score: acc_score + e.total_score }
      self.total += e.total_score
    }
  end
end
