class Game < ApplicationRecord

  belongs_to :frame, optional: true, dependent: :destroy
  has_many :frames, -> { order('index asc') }, dependent: :destroy

  after_create_commit :start

  def play(pins_dropped)
    if frame.play(pins_dropped.to_i)
      last_index = frame.index
      if play_on_next_frame?
        self.frame = frames[frame.index + 1]
        self.save
      end
      calculate_pending(last_index, pins_dropped.to_i)
      return true
    else
      if throw_after_end_game?
        errors[:base] << "Game has ended"
      end
      return false
    end
  end

  def play_on_next_frame?
    frame.index != 9 && !frame.has_next_ball?
  end

  def add_extra_score(index, pins_dropped)
    frames[index].add_extra_score(pins_dropped)
  end

  def calculate_pending(index, pins_dropped)
    first_frame_idx = index - 1
    second_frame_idx = index - 2
    if first_frame_idx >= 0
      add_extra_score(first_frame_idx, pins_dropped)
    end
    if second_frame_idx >= 0
      add_extra_score(second_frame_idx, pins_dropped)
    end
  end

  def create_frames
    10.times.each {|i| frames.create(index: i) }
  end

  def start
    create_frames
    update(frame: frames[0])
  end

  def throw_after_end_game?
    frame.is_last_frame? && frame.current_throw > frame.max_throws
  end
end
