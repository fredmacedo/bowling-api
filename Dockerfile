FROM ruby:2.7.1-alpine

RUN apk update && apk add build-base postgresql-dev tzdata

RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .

ADD . /app
CMD bundle exec rails s -p 3000 -b '0.0.0.0'
