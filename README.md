# 🚀 Bowling

## Setup
You can choose what you feel is best:


[Local Setup Instructions](docs/SETUP.md)

[Docker Setup Instructions](docs/DOCKER.md)

[API DOCS](docs/API.md)

API Service is deployed at: https://pacific-brook-08548.herokuapp.com/


🚀🚀 API can receive requests at host [http://localhost:3000/api](http://localhost:3000/api)
